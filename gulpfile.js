var gulp = require('gulp'),
    sass = require('gulp-sass')(require('sass')),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync')
// notify = require('gulp-notify')


gulp.task('styles', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('scripts', function () {
    return gulp.src('src/js/main.js')
        .pipe(sourcemaps.init())
        .pipe(rename('scripts.min.js'))
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('scriptsVendor', function () {
    return gulp.src('src/js/tabs.js')
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({ stream: true }))
})


gulp.task('html', function () {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('img', function () {
    return gulp.src([
        'src/img/**.jpg',
        'src/img/**.png',
        'src/img/**.jpeg',
        'src/img/*.svg',
        'src/img/**/*.jpg',
        'src/img/**/*.png',
        'src/img/**/*.jpeg'
    ])
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'dist'
        },
        notify: false
    })
})
gulp.task('watch', function() {
	gulp.watch('src/scss/**/*.scss', gulp.parallel('styles'));
	gulp.watch('src/index.html', gulp.parallel('html')); 
	gulp.watch('src/js/main.js', gulp.parallel('scripts')); 
    gulp.watch('src/js/tabs.js', gulp.parallel('scriptsVendor'))
    gulp.watch('src/img/**/*', gulp.parallel('img'))
});

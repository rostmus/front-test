const tabHeader = document.getElementsByClassName('tab-header')
const tabContent = document.getElementsByClassName('tab-content')

const tabText = document.querySelector('.tab-header-text')
const tabHeaderWrap = document.querySelector('.tab-header-wrap')

for (let i = 0; i < tabHeader.length; i++) {
  tabHeader[i].addEventListener('click', activateTab)

  function activateTab() {

    for (let z = 0; z < tabHeader.length; z++) {
      tabHeader[z].classList.remove('active')
      tabContent[z].classList.remove('active')
    }
    this.classList.add('active')
    let findEl = Array.from(tabHeader).findIndex((el) => el.classList.contains('active'))
    tabContent[findEl].classList.add('active')
    if (tabText.classList.contains('active')) {
      tabText.innerHTML = this.textContent
      tabHeaderWrap.style.display = 'none'
      tabText.classList.remove('active')
    }

  }
}

const svg = document.querySelector('svg')
document.addEventListener('scroll', scrollDocument)
function scrollDocument() {
  const header = document.querySelector('.header')
  const headerTop = header.getBoundingClientRect().top
  let windowScroll = window.pageYOffset
  header.style.position = 'fixed'
  header.style.height = 70 + 'px'
  // (300 - windowScroll + 'px')
  svg.style.height = 70 + 'px'
  if (windowScroll < 10) {
    header.style.height = 110 + 'px'
    header.style.position = 'inherit'
  }
}

const contentText = document.querySelector('.content-text')
const headerMenu = document.querySelector('.header-menu')
contentText.addEventListener('click', headerMenuClick)
function headerMenuClick() {
  if (headerMenu.classList.contains('active')) {
    headerMenu.style.display = 'none'
    headerMenu.classList.remove('active')
    return false
  }
  headerMenu.style.display = 'block'
  headerMenu.classList.add('active')

}

const headerMenuItem = document.getElementsByClassName('header-menu-item')
for (let j = 0; j < headerMenuItem.length; j++) {
  headerMenuItem[j].addEventListener('click', headerMenuItemCkick)
  function headerMenuItemCkick() {
    headerMenu.classList.remove('active')
    contentText.innerHTML = this.textContent
    headerMenu.style.display = 'none'
  }
}


const image_1 = document.querySelector('.image-1');

image_1.addEventListener('mousedown', function (evt) {
  evt.preventDefault();
  let startCoords = {
    x: evt.clientX,
    y: evt.clientY
  };
  let onMouseMove = function (moveEvt) {
    moveEvt.preventDefault();
    let shift = {
      x: startCoords.x - moveEvt.clientX,
      y: startCoords.y - moveEvt.clientY
    };
    startCoords = {
      x: moveEvt.clientX,
      y: moveEvt.clientY
    };
    image_1.style.top = (image_1.offsetTop - shift.y) + 'px';
    image_1.style.left = (image_1.offsetLeft - shift.x) + 'px';
  };
  let onMouseUp = function (upEvt) {
    upEvt.preventDefault();
    document.removeEventListener('mousemove', onMouseMove);
    document.removeEventListener('mouseup', onMouseUp);
  };
  document.addEventListener('mousemove', onMouseMove);
  document.addEventListener('mouseup', onMouseUp);
});


$('.image-contain-2').mousemove(function (event) {
  let border = 50
  let x = event.pageX - $(this).offset().left
  let y = event.pageY - $(this).offset().top
  let newx = (x / $(this).width()) * ($('.image-2', this).width() - $(this).width() + 2 * border) - border
  let newy = (y / $(this).height()) * ($('.image-2', this).height() - $(this).height() + 2 * border) - border
  $('img', this).css({ left: - newx, top: -newy })
})


tabText.addEventListener('click', tabClick)
function tabClick() {
  if (this.classList.contains('active')) {
    tabHeaderWrap.style.display = 'none'
    this.classList.remove('active')

    return false
  }
  this.classList.add('active')
  tabHeaderWrap.style.display = 'flex'


}